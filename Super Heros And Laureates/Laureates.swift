//
//  Laureates.swift
//  Super Heros And Laureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Lakshmi. All rights reserved.
//

import Foundation
//import UIKit

class Laureates{
    
    class LaureatesModel {
        var firstName:String
        var surName:String
        var bornDate:String
        var diedDate:String
        init(firstName:String, surName:String,bornDate:String,diedDate:String) {
            self.firstName = firstName
            self.surName = surName
            self.bornDate = bornDate
            self.diedDate = diedDate
        }
    }
    
//    private init(){}
    
    static let shared = Laureates()
    var finalLaureatesData:[LaureatesModel] = []
    //    var dummy:[LaureatesModel]?
    func fetchLaureates() -> Void {
        if let url = Bundle.main.url(forResource: "laureates", withExtension: "json"){
        
        let urlSession = URLSession.shared
        urlSession.dataTask(with: url, completionHandler: displayLaureatesData).resume()
        }
        else{
            print("No File")
            
        }
    }
    
    // callback function, once the URL has completed
    func displayLaureatesData(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        var laureates:[[String:Any]]!
        do {
            try laureates = JSONSerialization.jsonObject(with: data!, options: .allowFragments)  as?  [[String:Any]]
            if laureates != nil {
                for i in 0..<laureates.count{
                    let laureatesArray = laureates[i]
                    let firstName = laureatesArray["firstname"] as? String
                    let surName = laureatesArray["surname"] as? String
                    let bornDate = (laureatesArray["born"] as? String)!
                    let diedDate = (laureatesArray["died"] as? String)!
                    self.finalLaureatesData.append(LaureatesModel.init(firstName: firstName ?? "", surName: surName ?? "", bornDate: bornDate, diedDate: diedDate))
               }
            NotificationCenter.default.post(name: Notification.Name("Laureates Retrieved"), object: nil)
            }
            
        } catch {
            print(error)
        }
    }
    
}
