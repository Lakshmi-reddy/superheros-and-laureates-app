//
//  SuperHeros.swift
//  Super Heros And Laureates
//
//  Created by Student on 4/12/19.
//  Copyright © 2019 Lakshmi. All rights reserved.
//

import Foundation


class SuperHeros{
    
    static let shared = SuperHeros()
    var members:[Members] = []
    func fetchSuperHero() -> Void {
        if  let url = Bundle.main.url(forResource: "squad", withExtension: "json"){
        let urlSession = URLSession.shared
        urlSession.dataTask(with: url, completionHandler: superHeroDetails).resume()
        }
        else{
            print("No File in Directory")
        }
    }
    
    // callback function, once the URL has completed
    func superHeroDetails(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        do {
            let decoder = JSONDecoder()
            let superhero = try decoder.decode(SuperHero.self, from: data!)
            members = superhero.members
            NotificationCenter.default.post(name: Notification.Name("Heros Retrieved"), object: nil)
        }
            
        catch {
            print(error)
        }
    }
}

class SuperHero: Codable {
    var members:[Members]
}

class Members: Codable{
    var name:String
    var secretIdentity:String
    var powers:[String]
}
